package spaceships;


import java.util.ArrayList;
import java.util.Objects;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{

	@Override
	public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
		int most = 0;
		int id=0;
		for (int i = 0; i < ships.size(); i++) {
			if (ships.get(i).getFirePower()>most && ships.get(i).getFirePower()!=0){
				most = ships.get(i).getFirePower();
				id =i;
			}
		}
		if (most!=0){
			return ships.get(id);
		}
		return null;
	}

	@Override
	public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
		for (int i = 0; i < ships.size(); i++) {
			if (ships.get(i).getName().equals(name)){
				return ships.get(i);
			}
		}
		return null;
	}

	@Override
	public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
		ArrayList<Spaceship> shipsNeed = new ArrayList<>();
		if (Objects.nonNull(ships)){
			for (int i = 0; i < ships.size(); i++) {
				if (ships.get(i).getCargoSpace() >= cargoSize){
					shipsNeed.add(ships.get(i));
				}
			}
		}
		return shipsNeed;
	}


	@Override
	public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
		ArrayList<Spaceship> shipsNeed = new ArrayList<>();
		if (Objects.nonNull(ships)) {
			for (int i = 0; i < ships.size(); i++) {
				if (ships.get(i).getFirePower() == 0) {
					shipsNeed.add(ships.get(i));
				}
			}
		}
		return shipsNeed;
	}
}
