package Test;

import spaceships.*;
import java.util.ArrayList;
import java.util.Objects;

public class CommandCenterTest {

	SpaceshipFleetManager commandCenter = new CommandCenter();


	public static void main(String[] args) {
		int count =  0;

		CommandCenterTest commandCenterTest = new CommandCenterTest();

		System.out.println("Test 1 " + (commandCenterTest.isGetAllShipsWithEnoughCargoSpace_ReallyHaveEnoughCargoSpace()?
				"OK":"NO"));
		if (commandCenterTest.isGetAllShipsWithEnoughCargoSpace_ReallyHaveEnoughCargoSpace()){
			count++;
		}


		System.out.println("Test 2 " + (commandCenterTest.isGetAllShipsWithEnoughCargoSpace_HaveNo()?
				"OK":"NO"));
		if (commandCenterTest.isGetAllShipsWithEnoughCargoSpace_HaveNo()){
			count++;
		}
		
		System.out.println("Test 3 " + (commandCenterTest.getMostPowerfulShip_ReallyMostPowerful()?
				"OK":"NO"));
		if (commandCenterTest.getMostPowerfulShip_ReallyMostPowerful()){
			count++;
		}

		System.out.println("Test 3.5 " + (commandCenterTest.getMostPowerfulShip_AnyEqualsFP()?
				"OK":"NO"));
		if (commandCenterTest.getMostPowerfulShip_AnyEqualsFP()){
			count++;
		}
		
		System.out.println("Test 4 " + (commandCenterTest.getMostPowerfulShip_NoShipsWithFP()?
				"OK":"NO"));
		if (commandCenterTest.getMostPowerfulShip_NoShipsWithFP()){
			count++;
		}
		
		System.out.println("Test 5 " + (commandCenterTest.getShipByName_ShipIsHave()?
				"OK":"NO"));
		if (commandCenterTest.getShipByName_ShipIsHave()){
			count++;
		}
		
		System.out.println("Test 6 " + (commandCenterTest.getShipByName_ShipIsNotHave()?
				"OK":"NO"));
		if (commandCenterTest.getShipByName_ShipIsNotHave()){
			count++;
		}
		
		System.out.println("Test 7 " + (commandCenterTest.getAllCivilianShips_HaveCivilianShips()?
				"OK":"NO"));
		if (commandCenterTest.getAllCivilianShips_HaveCivilianShips()){
			count++;
		}
		
		System.out.println("Test 8 " + (commandCenterTest.getAllCivilianShips_HaveNotCivilianShips()?
				"OK":"NO"));
		if (commandCenterTest.getAllCivilianShips_HaveNotCivilianShips()){
			count++;
		}
		
		System.out.println(count);


	}
	


	public boolean isGetAllShipsWithEnoughCargoSpace_ReallyHaveEnoughCargoSpace() {
		ArrayList<Spaceship> ships = new ArrayList<>();
		ships.add(new Spaceship("a",1,100,0 ));
		ships.add(new Spaceship("b",1,1,0 ));
		ships.add(new Spaceship("c",1,10,0 ));
		ships.add(new Spaceship("d",1,101,0 ));


		ArrayList<Spaceship> ships2 =commandCenter.getAllShipsWithEnoughCargoSpace(ships,11);

		for (int i = 0; i < ships2.size(); i++) {
			if (!(ships2.get(i).getCargoSpace()>=11)){
				return false;
			}
		}
		return  true;
	}

	public boolean isGetAllShipsWithEnoughCargoSpace_HaveNo(){
		ArrayList<Spaceship> ships = new ArrayList<>();

		ships.add(new Spaceship("a",1,0,0 ));
		ships.add(new Spaceship("b",1,1,0 ));
		ships.add(new Spaceship("c",1,9,0 ));
		ships.add(new Spaceship("d",1,7,0 ));

		ArrayList<Spaceship> ships2 =commandCenter.getAllShipsWithEnoughCargoSpace(ships,11);


		return ships2.isEmpty();
	}

	public boolean getMostPowerfulShip_ReallyMostPowerful(){

		ArrayList<Spaceship> ships = new ArrayList<>();

		ships.add(new Spaceship("a",223,100,0 ));
		ships.add(new Spaceship("b",0,1,0 ));
		ships.add(new Spaceship("c",100,10,0 ));
		ships.add(new Spaceship("d",1005,101,0 ));

		return commandCenter.getMostPowerfulShip(ships).getFirePower()==1005;
	}

	public boolean getMostPowerfulShip_AnyEqualsFP(){

		ArrayList<Spaceship> ships = new ArrayList<>();

		ships.add(new Spaceship("a",100,100,0 ));
		ships.add(new Spaceship("b",100,1,0 ));
		ships.add(new Spaceship("c",100,10,0 ));
		ships.add(new Spaceship("d",100,101,0 ));

		return commandCenter.getMostPowerfulShip(ships).getName().equals("a");
	}

	public boolean getMostPowerfulShip_NoShipsWithFP(){

		ArrayList<Spaceship> ships = new ArrayList<>();
		ships.add(new Spaceship("a",0,100,0 ));
		ships.add(new Spaceship("b",0,1,0 ));
		ships.add(new Spaceship("c",0,10,0 ));
		ships.add(new Spaceship("d",0,101,0 ));


		return Objects.isNull(commandCenter.getMostPowerfulShip(ships));

	}

	public boolean getShipByName_ShipIsHave(){

		ArrayList<Spaceship> ships = new ArrayList<>();
		ships.add(new Spaceship("a",223,100,0 ));
		ships.add(new Spaceship("b",0,1,0 ));
		ships.add(new Spaceship("c",-100,10,0 ));
		ships.add(new Spaceship("d",1005,101,0 ));

		return commandCenter.getShipByName(ships, "a").getName().equals("a");
	}

	public boolean getShipByName_ShipIsNotHave(){

		ArrayList<Spaceship> ships = new ArrayList<>();
		ships.add(new Spaceship("a",223,100,0 ));
		ships.add(new Spaceship("b",0,1,0 ));
		ships.add(new Spaceship("c",-100,10,0 ));
		ships.add(new Spaceship("d",1005,101,0 ));

		return Objects.isNull(commandCenter.getShipByName(ships, "aa"));
	}

	public boolean getAllCivilianShips_HaveCivilianShips(){

		ArrayList<Spaceship> ships = new ArrayList<>();

		ships.add(new Spaceship("a",1,100,0 ));
		ships.add(new Spaceship("b",0,1,0 ));
		ships.add(new Spaceship("c",10,10,0 ));
		ships.add(new Spaceship("d",0,101,0 ));

		ArrayList<Spaceship> ships2 = commandCenter.getAllCivilianShips(ships);
		for (int i = 0; i < ships2.size(); i++) {
			if (ships2.get(i).getFirePower()!=0){
				return false;
			}
		}
		return true;
	}


	public boolean getAllCivilianShips_HaveNotCivilianShips(){

		ArrayList<Spaceship> ships = new ArrayList<>();

		ships.add(new Spaceship("a",1,100,0 ));
		ships.add(new Spaceship("b",2,1,0 ));
		ships.add(new Spaceship("c",10,10,0 ));
		ships.add(new Spaceship("d",4,101,0 ));


		return commandCenter.getAllCivilianShips(ships).isEmpty();
	}

}
