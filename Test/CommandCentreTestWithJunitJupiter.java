package Test;
import org.junit.jupiter.api.*;
import spaceships.*;

import java.util.ArrayList;

public class CommandCentreTestWithJunitJupiter {
	static int count;
	static SpaceshipFleetManager commandCenter;
	ArrayList<Spaceship> spaceshipArrayList;
	@BeforeAll
	static void startDate(){
		count = 0;
		commandCenter = new CommandCenter();
	}

	@BeforeEach
	void newDate(){
		spaceshipArrayList = new ArrayList<>();
	}

	@Test
	@DisplayName("Have not powerful ships")
	void getMostPowerfulShip_AllHaventFP(){
		spaceshipArrayList.add(new Spaceship("1",0,12,12));
		spaceshipArrayList.add(new Spaceship("2",0,12,12));
		spaceshipArrayList.add(new Spaceship("3",0,12,12));
		spaceshipArrayList.add(new Spaceship("4",0,12,12));
		spaceshipArrayList.add(new Spaceship("5",0,12,12));
		Assertions.assertNull(commandCenter.getMostPowerfulShip(spaceshipArrayList));
	}


	@Test
	@DisplayName("Have most powerful ships")
	void getMostPowerfulShip_HaveShip(){
		spaceshipArrayList.add(new Spaceship("1",2,12,12));
		spaceshipArrayList.add(new Spaceship("2",5,12,12));
		spaceshipArrayList.add(new Spaceship("3",12,12,12));
		spaceshipArrayList.add(new Spaceship("4",1233,12,12));
		spaceshipArrayList.add(new Spaceship("5",123,12,12));
		Assertions.assertEquals(1233,commandCenter.getMostPowerfulShip(spaceshipArrayList).getFirePower());
	}


	@Test
	@DisplayName("any ships with equals FP")
	void getMostPowerfulShip_Similar(){
		spaceshipArrayList.add(new Spaceship("1",9,12,12));
		Spaceship sp = new Spaceship("2",10,12,12);
		spaceshipArrayList.add(sp);
		spaceshipArrayList.add(new Spaceship("3",10,12,12));
		spaceshipArrayList.add(new Spaceship("4",10,12,12));
		spaceshipArrayList.add(new Spaceship("5",10,12,12));
		Assertions.assertEquals(sp,commandCenter.getMostPowerfulShip(spaceshipArrayList));
	}

	@Test
	@DisplayName("Have ship by name")
	void getShipByName_HaveShip(){
		spaceshipArrayList.add(new Spaceship("Toronto",2,12,12));
		spaceshipArrayList.add(new Spaceship("Tokyo",5,12,12));
		spaceshipArrayList.add(new Spaceship("Hotel",12,12,12));
		spaceshipArrayList.add(new Spaceship("B8",1233,12,12));
		spaceshipArrayList.add(new Spaceship("HR",123,12,12));
		Assertions.assertEquals( "Tokyo",commandCenter.getShipByName(spaceshipArrayList,"Tokyo").getName());
	}


	@Test
	@DisplayName("Have no ship by name")
	void getShipByName_NoShip(){
		spaceshipArrayList.add(new Spaceship("Toronto",32,12,12));
		spaceshipArrayList.add(new Spaceship("Tokyo",5,12,12));
		spaceshipArrayList.add(new Spaceship("Hotel",12,12,12));
		spaceshipArrayList.add(new Spaceship("B8",1233,12,12));
		spaceshipArrayList.add(new Spaceship("HR",123,12,12));
		Assertions.assertNull( commandCenter.getShipByName(spaceshipArrayList,"zxc"));
	}


	@Test
	@DisplayName("Have ships with more CS")
	void getAllShipsWithEnoughCargoSpace_HaveSips(){
		spaceshipArrayList.add(new Spaceship("Toronto",32,119,12));
		spaceshipArrayList.add(new Spaceship("HR",123,1,12));

		Spaceship sp1 = new Spaceship("Tokyo",5,122,12);
		Spaceship sp2 = new Spaceship("Hotel",12,123,12);
		Spaceship sp3 = new Spaceship("B8",1233,121,12);

		ArrayList<Spaceship> spaceships = new ArrayList<>();
		spaceships.add(sp1);
		spaceships.add(sp2);
		spaceships.add(sp3);

		spaceshipArrayList.add(sp1);
		spaceshipArrayList.add(sp2);
		spaceshipArrayList.add(sp3);
		Assertions.assertEquals( spaceships,commandCenter.getAllShipsWithEnoughCargoSpace(spaceshipArrayList, 120));
	}


	@Test
	@DisplayName("Have no ships with more CS")
	void getAllShipsWithEnoughCargoSpace_HaveNoSips(){
		spaceshipArrayList.add(new Spaceship("Toronto",32,119,12));
		spaceshipArrayList.add(new Spaceship("HR",123,1,12));
		spaceshipArrayList.add(new Spaceship("Tokyo",5,122,12));
		spaceshipArrayList.add(new Spaceship("Hotel",12,123,12));
		spaceshipArrayList.add(new Spaceship("B8",1233,121,12));
		Assertions.assertEquals(commandCenter.getAllShipsWithEnoughCargoSpace(spaceshipArrayList, 999).size(), 0);
	}


	@Test
	@DisplayName("Have civilian ships")
	void getAllCivilianShips_HaveShips(){
		ArrayList<Spaceship> spaceships = new ArrayList<>();
		spaceships.add(new Spaceship("Toronto",0,119,12));
		spaceshipArrayList.add(spaceships.get(0));

		spaceshipArrayList.add(new Spaceship("HR",123,1,12));

		spaceships.add(new Spaceship("Tokyo",0,122,12));
		spaceshipArrayList.add(spaceships.get(1));

		spaceshipArrayList.add(new Spaceship("Hotel",12,123,12));

		spaceships.add(new Spaceship("B8",0,121,12));
		spaceshipArrayList.add(spaceships.get(2));
		Assertions.assertEquals(spaceships, commandCenter.getAllCivilianShips(spaceshipArrayList));
	}



	@Test
	@DisplayName("Have not civilian ships")
	void getAllCivilianShips_HaveNotShips(){
		ArrayList<Spaceship> spaceships = new ArrayList<>();
		spaceshipArrayList.add(new Spaceship("Toronto",12,119,12));

		spaceshipArrayList.add(new Spaceship("HR",123,1,12));

		spaceshipArrayList.add(new Spaceship("Tokyo",1230,122,12));

		spaceshipArrayList.add(new Spaceship("Hotel",12,123,12));

		spaceshipArrayList.add(new Spaceship("B8",120,121,12));

		Assertions.assertEquals(0, commandCenter.getAllCivilianShips(spaceshipArrayList).size());
	}

}
